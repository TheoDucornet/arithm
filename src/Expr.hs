-- |
-- Module: Expr
-- This is my Expr module
module Expr where

-- | Define expr for Add and Mul multiple Values
data Expr
    = Val Int
    | Add Expr Expr
    | Mul Expr Expr
    deriving (Eq, Show)

