module EvalSpec (main, spec) where

import Test.Hspec

import Eval
import Expr

main :: IO ()
main = hspec spec

spec :: Spec
spec = 
    describe "eval" $ do
        it "0" $ eval (Val 0) `shouldBe` 0
        it "42" $ eval (Val 42) `shouldBe` 42
        it "mul 2 21" $ eval (Mul (Val 2) (Val 21)) `shouldBe` 42